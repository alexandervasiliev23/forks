<?php

namespace app\commands;

use app\components\Grabber\Grabber;
use app\components\Parser\Parser;
use app\repositories\MatchRepository;
use DateTime;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverKeys;
use http\Exception\RuntimeException;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Throwable;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Exception;
use function class_exists;
use function exec;
use function file_exists;
use function file_put_contents;
use function key_exists;
use function microtime;
use function print_r;
use function sleep;
use function ucfirst;
use function var_dump;
use const PHP_EOL;

/**
 * Class GrabberController
 * @package app\commands
 */
class GrabberController extends Controller
{
    private array $params;

    public function actionListen(string $bookmakerName)
    {
        $bookmakerParams = $this->params['bookmakers'][$bookmakerName] ?? null;
        if (!$bookmakerParams) {
            throw new InvalidArgumentException("Bookmaker '$bookmakerName' not found");
        }

        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments([
            '--proxy-server=socks5://tor:9150',
            '--no-sandbox',
//            '--disable-dev-shm-usage',
//            '--disable-gpu',
//            '--disable-setuid-sandbox',
//            '--headless',
        ]);
        $chromeOptions->setExperimentalOption('excludeSwitches', ['enable-automation']);

        $desiredCapabilities = DesiredCapabilities::chrome();
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $driver = RemoteWebDriver::create('http://localhost:9515', $desiredCapabilities);
        $driver->get($bookmakerParams['url']);

        while (true) {
            echo PHP_EOL . '==============================' . PHP_EOL;
            $time = (new DateTime())->format('H:i:s');
            try {
                echo $time . PHP_EOL;

//                $content = $driver->findElement(WebDriverBy::xpath($bookmakerParams['xpath']))->getText();

                $html = $driver
                    ->findElement(WebDriverBy::tagName('body'))
                    ->getAttribute('innerHTML');

                file_put_contents(Yii::getAlias('@runtime') . "/files/$bookmakerName.html", $html);

                $parserStrategyFullClassName = 'app\\components\\Parser\\ParserStrategy\\' . ucfirst($bookmakerName) . 'ParserStrategy';
                if (!class_exists($parserStrategyFullClassName)) {
                    continue;
                }

                $parser                = new Parser(new $parserStrategyFullClassName);
                $parsedMatchCollection = $parser->parse($html);

                $matchRepository = new MatchRepository();

                $transaction = Yii::$app->getDb()->beginTransaction();
                if (!$transaction) {
                    throw new RuntimeException('Could not create transaction');
                }
                $matchRepository->clear($bookmakerParams['bookmakerId']);
                $matchRepository->save($parsedMatchCollection);
                $transaction->commit();
            } catch (Throwable $exception) {
                $transaction->rollBack();
                echo $exception->getMessage() . PHP_EOL;
            }

            sleep(1);
        }

        exit(0);
    }

    /**
     * GrabberController constructor.
     *
     * @param       $id
     * @param       $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->params = Yii::$app->params;
    }

    /**
     * @return int
     * @throws Exception
     * @throws Throwable
     */
    public function actionGrab(): int
    {
        $bookmakersToGrab = [
            'betfair',
            'pinnacle',
//            'sbobet',
        ];

        foreach ($bookmakersToGrab as $bookmakerName) {
            $start       = microtime(true);
            $grabbedHtml = (new Grabber($this->params['grabber']['driverHost']))
                ->setProxyServer($this->params['grabber']['proxyServer'])
                ->setScreenshotPath($this->params['bookmakerSettings'][$bookmakerName]['screenshot'])
                ->setWaitingForCssSelector($this->params['bookmakerSettings'][$bookmakerName]['waitingForCssSelector'])
                ->grab($this->params['bookmakerSettings'][$bookmakerName]['url']);

            if (!$grabbedHtml) {
                continue;
            }

            file_put_contents(Yii::getAlias('@runtime') . "/files/$bookmakerName.html", $grabbedHtml);

            $parserStrategyFullClassName = 'app\\components\\Parser\\ParserStrategy\\' . ucfirst($bookmakerName) . 'ParserStrategy';
            if (!class_exists($parserStrategyFullClassName)) {
                continue;
            }

            $parser                = new Parser(new $parserStrategyFullClassName);
            $parsedMatchCollection = $parser->parse($grabbedHtml);

            (new MatchRepository())->save($parsedMatchCollection);

            echo $bookmakerName . ': ' . (microtime(true) - $start) . PHP_EOL;
        }

        return ExitCode::OK;
    }
}
