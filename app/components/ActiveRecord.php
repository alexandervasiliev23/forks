<?php

namespace app\components;

use Yii;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\Response;

/**
 * Class ActiveRecord
 * @package app\components
 */
class ActiveRecord extends \yii\db\ActiveRecord
{
    /**
     *
     */
    public function loadDataFromPost(): bool
    {
        return $this->load(Yii::$app->getRequest()->post(), '');
    }

    /**
     * @throws HttpException
     */
    public function validateOrDieApi(): void
    {
        if (!$this->validate()) {
            Yii::$app->getResponse()->format = Response::FORMAT_JSON;

            throw new HttpException(400, Json::encode($this->getErrorSummary(true)));
        }
    }
}
