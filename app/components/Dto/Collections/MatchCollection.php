<?php

namespace app\components\Dto\Collections;

use app\components\Dto\Match;
use JsonSerializable;

/**
 * Class MatchCollection
 * @package app\components\Match
 */
class MatchCollection implements JsonSerializable
{
    /** @var Match[] */
    private array $matches = [];

    /**
     * @param Match $match
     */
    public function add(Match $match): void
    {
        $this->matches[] = $match;
    }

    /**
     * @return Match[]|array
     */
    public function getAll(): array
    {
        return $this->matches;
    }

    /**
     * @return Match[]|array|mixed
     */
    public function jsonSerialize()
    {
        return $this->matches;
    }
}
