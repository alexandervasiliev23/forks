<?php

namespace app\components\Dto;

use app\components\Dto\Participant;
use JsonSerializable;

/**
 * Class Match
 * @package app\components\Match
 */
class Match implements JsonSerializable
{
    private Participant $participant1;
    private Participant $participant2;
    private int         $bookmakerId;

    /**
     * Match constructor.
     *
     * @param Participant $participant1
     * @param Participant $participant2
     * @param int         $bookmakerId
     */
    public function __construct(Participant $participant1, Participant $participant2, int $bookmakerId)
    {
        $this->participant1 = $participant1;
        $this->participant2 = $participant2;
        $this->bookmakerId  = $bookmakerId;
    }

    /**
     *
     */
    public function swapParticipants(): void
    {
        $participant2       = $this->participant2;
        $this->participant2 = $this->participant1;
        $this->participant1 = $participant2;
    }

    /**
     * @return Participant
     */
    public function getParticipant1(): Participant
    {
        return $this->participant1;
    }

    /**
     * @return Participant
     */
    public function getParticipant2(): Participant
    {
        return $this->participant2;
    }

    /**
     * @return int
     */
    public function getBookmakerId(): int
    {
        return $this->bookmakerId;
    }


    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'bookmakerId'  => $this->getBookmakerId(),
            'participant1' => $this->getParticipant1(),
            'participant2' => $this->getParticipant2(),
        ];
    }
}
