<?php

namespace app\components\Dto;

use JsonSerializable;

/**
 * Class Participant
 */
class Participant implements JsonSerializable
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var float
     */
    private float $rate;

    /**
     * Player constructor.
     *
     * @param string $name
     * @param float  $rate
     */
    public function __construct(string $name, float $rate)
    {
        $this->name = $name;
        $this->rate = $rate;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'rate' => $this->getRate(),
        ];
    }
}
