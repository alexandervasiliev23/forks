<?php

namespace app\components\ForksSeeker;

use app\components\Dto\Collections\MatchCollection;
use app\components\Dto\Match;
use app\components\ParticipantsMatcher\ParticipantMatcher;
use PDO;
use yii\db\Exception;
use yii\db\Query;
use function strtolower;

/**
 * Class ForksSeeker
 * @package app\components\ForksFinder
 */
class ForksSeeker
{
    private ParticipantMatcher $participantMatcher;
    private array              $bookmakers;
    private array              $forks = [];

    /**
     * ForksSeeker constructor.
     *
     * @param ParticipantMatcher $participantMatcher
     */
    public function __construct(ParticipantMatcher $participantMatcher)
    {
        $this->participantMatcher = $participantMatcher;
    }

    /**
     * @param MatchCollection[] $matchCollections
     *
     * @return array
     * @throws Exception
     */
    public function seek(array $matchCollections): array
    {
        $this->bookmakers = (new Query())
            ->from('bookmaker')
            ->createCommand()
            ->queryAll([PDO::FETCH_KEY_PAIR]);

        $matchCollectionsCount = count($matchCollections);

        foreach ($matchCollections as $i => $matchCollection) {
            for ($j = $i + 1; $j < $matchCollectionsCount; $j++) {
                foreach ($matchCollection->getAll() as $match1) {
                    foreach ($matchCollections[$j]->getAll() as $match2) {
                        $this->getForkIfExists($match1, $match2);
                    }
                }
            }
        }

        return $this->forks;
    }

    /**
     * @param Match $match1
     * @param Match $match2
     *
     * @return void
     */
    private function getForkIfExists(Match $match1, Match $match2): void
    {
        $firstBookmaker  = $this->bookmakers[$match1->getBookmakerId()];
        $secondBookmaker = $this->bookmakers[$match2->getBookmakerId()];

        $firstBookmakerParticipants      = [
            $match1->getParticipant1()->getName(),
            $match1->getParticipant2()->getName(),
        ];
        $secondBookmakerParticipantNames = $this->participantMatcher->getParticipantNames()[strtolower($secondBookmaker)];
        $secondBookmakerParticipants     = [
            $secondBookmakerParticipantNames[$match2->getParticipant1()->getName()][strtolower($firstBookmaker) . '_name'],
            $secondBookmakerParticipantNames[$match2->getParticipant2()->getName()][strtolower($firstBookmaker) . '_name'],
        ];

        $isFork = false;
        if (
            $firstBookmakerParticipants[0] === $secondBookmakerParticipants[0]
            && $firstBookmakerParticipants[1] && $secondBookmakerParticipants[1]
        ) {
            $match2->swapParticipants();
            $isFork = true;
        }

        if (!$isFork
            && $firstBookmakerParticipants[0] === $secondBookmakerParticipants[1]
            && $firstBookmakerParticipants[0] && $secondBookmakerParticipants[1]
        ) {
            $isFork = true;
        }

        if ($isFork) {
            $this->forks[] = [
                'bookmaker1' => $match1,
                'bookmaker2' => $match2,
            ];
        }
    }
}
