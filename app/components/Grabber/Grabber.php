<?php

namespace app\components\Grabber;

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Exception\TimeoutException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Throwable;
use Yii;
use function count;

/**
 * Class Grabber
 * @package app\components\Grabber
 */
class Grabber
{
    private string $proxyServer;
    private string $driverHost;
    private string $waitingForCssSelector;
    private string $screenshotPath;

    /**
     * Grabber constructor.
     *
     * @param string $driverHost
     */
    public function __construct(string $driverHost)
    {
        $this->driverHost = $driverHost;
    }

    /**
     * @param string $url
     *
     * @return string
     * @throws Throwable
     */
    public function grab(string $url): string
    {
        $html = '';

        try {
            $driver = $this->prepareWebDriver();

            $driver->get($url);

            if (!empty($this->waitingForCssSelector)) {
                $driver->wait()->until(
                    function () use ($driver) {
                        $elements = $driver->findElements(WebDriverBy::cssSelector($this->waitingForCssSelector));

                        return count($elements) > 0;
                    },
                    'Error locating elements'
                );
            }

            if ($this->screenshotPath) {
                $fullFileName = Yii::getAlias('@runtime') . '/files/' . $this->screenshotPath;

                $driver->takeScreenshot($fullFileName);
            }

            $html = $driver
                ->findElement(WebDriverBy::tagName('body'))
                ->getAttribute('innerHTML');

            $driver->quit();
        } catch (TimeoutException $exception) {
            if (isset($driver)) {
                $driver->quit();
            }
        } catch (Throwable $exception) {
            if (isset($driver)) {
                $driver->quit();
            }
            throw $exception;
        }

        return $html;
    }

    /**
     * @param string $proxyServer
     *
     * @return $this
     */
    public function setProxyServer(string $proxyServer): self
    {
        $this->proxyServer = $proxyServer;

        return $this;
    }

    /**
     * @param string $waitingForCssSelector
     *
     * @return $this
     */
    public function setWaitingForCssSelector(string $waitingForCssSelector): self
    {
        $this->waitingForCssSelector = $waitingForCssSelector;

        return $this;
    }

    /**
     * @param string $screenshotPath
     *
     * @return $this
     */
    public function setScreenshotPath(string $screenshotPath): self
    {
        $this->screenshotPath = $screenshotPath;

        return $this;
    }

    /**
     * @return RemoteWebDriver
     */
    private function prepareWebDriver(): RemoteWebDriver
    {
        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments([
            '--no-sandbox',
            '--headless',
            '--disable-dev-shm-usage',
//            '--disable-gpu',
        ]);
        if ($this->proxyServer) {
            $chromeOptions->addArguments([
                '--proxy-server=' . $this->proxyServer,
            ]);
        }

        $desiredCapabilities = DesiredCapabilities::chrome();
        $desiredCapabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        return RemoteWebDriver::create($this->driverHost, $desiredCapabilities);
    }
}
