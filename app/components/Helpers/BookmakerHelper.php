<?php

namespace app\components\Helpers;

/**
 * Class BookmakerHelper
 * @package app\components\Helpers
 */
class BookmakerHelper
{
    // todo нужно сделать гибче
    public const PINNACLE_BOOKMAKER_ID = 1;
    public const BETFAIR_BOOKMAKER_ID  = 2;
    public const SBOBET_BOOKMAKER_ID   = 3;
}
