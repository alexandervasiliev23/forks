<?php

namespace app\components\Parser;

use app\components\Dto\Collections\MatchCollection;
use app\components\Parser\ParserStrategy\ParserStrategyInterface;

/**
 * Class Parser
 * @package app\components\Parser
 */
class Parser
{
    private ParserStrategyInterface $parserStrategy;

    /**
     * Parser constructor.
     *
     * @param ParserStrategyInterface $parserStrategy
     */
    public function __construct(ParserStrategyInterface $parserStrategy)
    {
        $this->parserStrategy = $parserStrategy;
    }

    /**
     * @param string $html
     *
     * @return MatchCollection
     */
    public function parse(string $html): MatchCollection
    {
        return $this->parserStrategy->parse($html);
    }
}
