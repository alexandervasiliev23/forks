<?php

namespace app\components\Parser\ParserStrategy;

use app\components\Dto\Collections\MatchCollection;
use app\components\Dto\Match;
use app\components\Dto\Participant;
use DOMDocument;
use DOMXPath;
use function libxml_use_internal_errors;

/**
 * Class AbstractParserStrategy
 * @package app\components\Parser\ParserStrategy
 */
abstract class AbstractParserStrategy implements ParserStrategyInterface
{
    protected MatchCollection $matchCollection;

    /**
     * AbstractParserStrategy constructor.
     */
    public function __construct()
    {
        $this->matchCollection = new MatchCollection();
    }

    /**
     * @param string $html
     *
     * @return DOMXPath
     */
    protected function prepareXpath(string $html): DOMXPath
    {
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));

        return new DOMXPath($dom);
    }

    /**
     * @param Participant $participant1
     * @param Participant $participant2
     */
    protected function addMatch(Participant $participant1, Participant $participant2): void
    {
        $match = new Match($participant1, $participant2, $this->getBookmakerId());
        $this->matchCollection->add($match);
    }

    /**
     * @return string
     */
    abstract protected function getBookmakerId(): string;
}
