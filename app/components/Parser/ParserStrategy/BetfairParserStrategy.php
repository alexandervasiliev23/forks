<?php

namespace app\components\Parser\ParserStrategy;

use app\components\Dto\Collections\MatchCollection;
use app\components\Dto\Participant;
use app\components\Helpers\BookmakerHelper;
use function count;
use function print_r;

/**
 * Class BetfairParserStrategy
 * @package app\components\Parser\ParserStrategy
 */
class BetfairParserStrategy extends AbstractParserStrategy
{
    /**
     * @param string $html
     *
     * @return MatchCollection
     */
    public function parse(string $html): MatchCollection
    {
        $xpath = $this->prepareXpath($html);

        $couponTable = $xpath->query('//table[contains(@class,"coupon-table")]');
        $tableBody   = $xpath->query('.//tbody', $couponTable[0]);
        $rows        = $xpath->query('.//tr', $tableBody[0]);

        foreach ($rows as $row) {
            $names = $xpath->query('.//li[contains(@class,"name")]', $row);

            $players = [];
            foreach ($names as $name) {
                $players[] = $name->nodeValue;
            }

            $prices = $xpath->query('.//button[contains(@class,"back-button")]', $row);

            $rates = [];
            foreach ($prices as $key => $price) {
                if (count($prices) === 3 && ($key !== 0 && $key !== 2)) {
                    continue;
                }
                $rates[] = $xpath->query('.//span[contains(@class,"bet-button-price")]', $price)[0]->nodeValue;
            }

            if ($players && $rates) {
                $firstPlayer  = new Participant($players[0], $rates[0]);
                $secondPlayer = new Participant($players[1], $rates[1] ?: 100500);
                $this->addMatch($firstPlayer, $secondPlayer);
            }
        }

        return $this->matchCollection;
    }

    /**
     * @return string
     */
    public function getBookmakerId(): string
    {
        return BookmakerHelper::BETFAIR_BOOKMAKER_ID;
    }
}
