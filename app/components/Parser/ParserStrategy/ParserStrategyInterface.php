<?php

namespace app\components\Parser\ParserStrategy;

use app\components\Dto\Collections\MatchCollection;

/**
 * Interface ParserStrategyInterface
 * @package app\components\Parser\ParserStrategy
 */
interface ParserStrategyInterface
{
    /**
     * @param string $html
     *
     * @return MatchCollection
     */
    public function parse(string $html): MatchCollection;
}
