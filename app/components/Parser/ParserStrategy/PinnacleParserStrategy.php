<?php

namespace app\components\Parser\ParserStrategy;

use app\components\Dto\Collections\MatchCollection;
use app\components\Dto\Participant;
use app\components\Helpers\BookmakerHelper;
use DOMElement;
use function count;
use function str_replace;
use function strpos;

/**
 * Class PinnacleParserStrategy
 * @package app\components\Parser\ParserStrategy
 */
class PinnacleParserStrategy extends AbstractParserStrategy
{
    /**
     * @param string $html
     *
     * @return MatchCollection
     */
    public function parse(string $html): MatchCollection
    {
        $xpath = $this->prepareXpath($html);

        $container = $xpath->query('//div[contains(@class,"style_vertical__")]');
        $rows      = $xpath->query('.//div[contains(@class,"style_row__")]', $container[0]);

        /** @var DOMElement $row */
        foreach ($rows as $row) {
            $participantsContainer = $xpath->query('.//div[contains(@class,"style_colGame__")]', $row);
            $participants          = $xpath->query('.//span[contains(@class,"style_participantName__")]', $participantsContainer[0]);
            $players               = [];
            foreach ($participants as $participant) {
                $players[] =  $participant->nodeValue;
            }

//            if (count($players) !== 2 || strpos($players[0], 'Match') === false) {
//                continue;
//            }

            $players[0] = str_replace(' (Match)', '', $players[0]);
            $players[1] = str_replace(' (Match)', '', $players[1]);

            $prices = $xpath->query('.//span[contains(@class,"price")]', $row);
            $rates  = [];
            foreach ($prices as $price) {
                $rates[] = $price->nodeValue;
            }

            if ($players && $rates) {
                $firstPlayer  = new Participant($players[0], $rates[0]);
                $secondPlayer = new Participant($players[1], $rates[1]);
                $this->addMatch($firstPlayer, $secondPlayer);
            }
        }

        return $this->matchCollection;
    }

    /**
     * @return string
     */
    public function getBookmakerId(): string
    {
        return BookmakerHelper::PINNACLE_BOOKMAKER_ID;
    }
}
