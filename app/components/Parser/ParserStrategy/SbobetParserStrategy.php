<?php

namespace app\components\Parser\ParserStrategy;

use app\components\Dto\Collections\MatchCollection;
use app\components\Helpers\BookmakerHelper;

/**
 * Class SbobetParserStrategy
 * @package app\components\Parser\ParserStrategy
 */
class SbobetParserStrategy extends AbstractParserStrategy
{
    /**
     * @param string $html
     *
     * @return MatchCollection
     */
    public function parse(string $html): MatchCollection
    {
        // todo написать парсер

        return $this->matchCollection;
    }

    /**
     * @return string
     */
    public function getBookmakerId(): string
    {
        return BookmakerHelper::SBOBET_BOOKMAKER_ID;
    }
}
