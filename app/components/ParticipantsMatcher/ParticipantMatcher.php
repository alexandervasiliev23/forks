<?php

namespace app\components\ParticipantsMatcher;

use PDO;
use yii\db\Exception;
use yii\db\Query;
use function array_flip;
use function str_replace;

/**
 * Class ParticipantsMatcher
 * @package app\components\ParticipantsMatcher
 */
class ParticipantMatcher
{
    private array $betfairNames  = [];
    private array $pinnacleNames = [];
    private array $sbobetNames   = [];

    private const PINNALCE_NAME = 'pinnacle_name';
    private const BETFAIR_NAME  = 'betfair_name';
    private const SBOBET_NAME   = 'sbobet_name';

    private const PINNALCE = 'pinnacle';
    private const BETFAIR  = 'betfair';
    private const SBOBET   = 'sbobet';

    /**
     * @return array
     */
    public function getParticipantNames(): array
    {
        return [
            self::PINNALCE => $this->pinnacleNames,
            self::BETFAIR  => $this->betfairNames,
            self::SBOBET   => $this->sbobetNames,
        ];
    }

    /**
     * @throws Exception
     */
    public function prepare(): void
    {
        $allParticipantNames = (new Query())
            ->from('participant')
            ->createCommand()
            ->queryAll();

        foreach ($allParticipantNames as $participantName) {
            $this->fillParticipantName(self::PINNALCE, $participantName);
            $this->fillParticipantName(self::BETFAIR, $participantName);
            $this->fillParticipantName(self::SBOBET, $participantName);
        }
    }

    /**
     * @param string $bookmaker
     * @param array  $participantName
     */
    private function fillParticipantName(string $bookmaker, array $participantName): void
    {
        switch ($bookmaker) {
            case self::PINNALCE:
                if ($participantName[self::PINNALCE_NAME]) {
                    $this->pinnacleNames[$participantName[self::PINNALCE_NAME]] = [
                        self::BETFAIR_NAME => $participantName[self::BETFAIR_NAME],
                        self::SBOBET_NAME  => $participantName[self::SBOBET_NAME],
                    ];
                }
                break;
            case self::BETFAIR:
                if ($participantName[self::BETFAIR_NAME]) {
                    $this->betfairNames[$participantName[self::BETFAIR_NAME]] = [
                        self::PINNALCE_NAME => $participantName[self::PINNALCE_NAME],
                        self::SBOBET_NAME   => $participantName[self::SBOBET_NAME],
                    ];
                }
                break;
            case self::SBOBET:
                if ($participantName[self::SBOBET_NAME]) {
                    $this->sbobetNames[$participantName[self::SBOBET_NAME]] = [
                        self::PINNALCE_NAME => $participantName[self::PINNALCE_NAME],
                        self::BETFAIR_NAME  => $participantName[self::BETFAIR_NAME],
                    ];
                }
                break;
        }
    }
}
