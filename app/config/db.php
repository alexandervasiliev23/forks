<?php

use yii\db\Connection;

return [
    'class'    => Connection::class,
    'dsn'      => 'pgsql:host=postgres;port=5432;dbname=forks',
    'username' => 'postgres',
    'password' => 'pgpass',
    'charset'  => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
