<?php

return [
    'bookmakers' => [
        'betfair'  => [
//            'url'   => 'https://www.betfair.com/exchange/plus/inplay/e-sports',
//            'url'   => 'https://www.betfair.com/exchange/plus/inplay/tennis',
//            'url'   => 'https://www.betfair.com/exchange/plus/inplay/baseball',
            'url'   => 'https://www.betfair.com/exchange/plus/en/tennis-betting-2',
            'xpath' => '//*[contains(@class, "coupon-table-mod")]',
            'bookmakerId' => 2
        ],
        'pinnacle' => [
//            'url'   => 'https://www.pinnacle.com/ru/esports/matchups/live',
//            'url'   => 'https://www.pinnacle.com/ru/baseball/matchups/live',
            'url'   => 'https://www.pinnacle.com/ru/tennis/matchups',
            'xpath' => '/html/body/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[2]/a[1]/span',
            'bookmakerId' => 1
        ],
        'mollybet' => [
            'url' => 'https://mollybet.com/trade',
            'bookmakerId' => 4
        ],
        'sbobet' => [
            'url'   => 'https://www.sbobet.com',
            'xpath' => '/html/body/div[2]/div/div/div/div[2]/div[2]/div/div/div/div[2]/div[2]/div/div[2]/div[2]/a[1]/span',
        ],
    ],


    'bookmakerSettings' => [
        'pinnacle' => [
            'url'                   => 'https://www.pinnacle.com/ru/esports/matchups/live',
            'waitingForCssSelector' => 'span.price',
            'screenshot'            => 'pinnacle_screenshot.png',
        ],
        'betfair'  => [
            'url'                   => 'https://www.betfair.com/exchange/plus/inplay/e-sports',
            'waitingForCssSelector' => 'button.bf-bet-button',
            'screenshot'            => 'betfair_screenshot.png',
        ],
        'sbobet'   => [
            'url'                   => 'https://www.sbobet.com/euro/e-sports',
//            'waitingForCssSelector' => 'div.odds',
            'waitingForCssSelector' => 'div.Container',
            'screenshot'            => 'sbobet_screenshot.png',
        ],
        'checkIp'  => [
            'url'                   => 'http://ip-api.com/json/',
            'waitingForCssSelector' => 'pre',
            'screenshot'            => 'check_ip_screenshot.png',
        ],
    ],
    'grabber'           => [
        'proxyServer' => 'socks5://tor:9150',
        'driverHost'  => 'http://chromedriver:4444',
    ],
];
