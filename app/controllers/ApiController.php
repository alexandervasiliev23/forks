<?php

namespace app\controllers;

use app\components\ForksSeeker\ForksSeeker;
use app\components\Grabber\Grabber;
use app\components\Helpers\BookmakerHelper;
use app\components\Helpers\ParamsHelper;
use app\components\ParticipantsMatcher\ParticipantMatcher;
use app\repositories\MatchRepository;
use PDO;
use Yii;
use yii\db\Exception;
use yii\db\Query;
use yii\rest\Controller;
use yii\web\Response;
use function array_merge;
use function base64_encode;
use function class_exists;
use function file_exists;
use function file_get_contents;
use function file_put_contents;
use function in_array;
use function ucfirst;

/**
 * Class ApiController
 * @package app\controllers
 */
class ApiController extends Controller
{
    private array $params;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->params = Yii::$app->params;
    }

    public function actionTest(): array
    {
        return [];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionIndex(): array
    {
        $betfairMatchCollection  = (new MatchRepository())->getLastMatchesByBookmaker(BookmakerHelper::BETFAIR_BOOKMAKER_ID);
        $pinnacleMatchCollection = (new MatchRepository())->getLastMatchesByBookmaker(BookmakerHelper::PINNACLE_BOOKMAKER_ID);
        $sbobetMatchCollection   = (new MatchRepository())->getLastMatchesByBookmaker(BookmakerHelper::SBOBET_BOOKMAKER_ID);

        $participantMather = new ParticipantMatcher();
        $participantMather->prepare();

        $forksSeeker = new ForksSeeker($participantMather);
        $forks       = $forksSeeker->seek([
            $betfairMatchCollection,
            $pinnacleMatchCollection,
            $sbobetMatchCollection,
        ]);

        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        $allMatches = array_merge(
            $betfairMatchCollection->getAll(),
            $pinnacleMatchCollection->getAll(),
            $sbobetMatchCollection->getAll()
        );

        foreach ($forks as $fork) {
            foreach ($allMatches as $key => $match) {
                if (in_array($match, $fork, true)) {
                    unset($allMatches[$key]);
                }
            }
        }

        $notAllocatedMatches = [];
        foreach ($allMatches as $match) {
            $notAllocatedMatches[$match->getBookmakerId()][] = $match;
        }

        $bookmakers = (new Query())
            ->select(['id', '*'])
            ->from('bookmaker')
            ->createCommand()
            ->queryAll([PDO::FETCH_UNIQUE | PDO::FETCH_ASSOC]);

        $sports = (new Query())
            ->select(['id', '*'])
            ->from('sport')
            ->createCommand()
            ->queryAll([PDO::FETCH_UNIQUE | PDO::FETCH_ASSOC]);

        return [
            'notAllocatedMatches' => $notAllocatedMatches,
            'forks'               => $forks,
            'dictionary'          => [
                'bookmakers' => $bookmakers,
                'sports'     => $sports,
            ],
        ];
    }

    /**
     * @return array|string[]
     */
    public function actionLastGrabbed(): array
    {
        $betfairFile  = Yii::getAlias('@runtime') . '/files/betfair_screenshot.png';
        $pinnacleFile = Yii::getAlias('@runtime') . '/files/pinnacle_screenshot.png';

        $betfairScreenshot  = null;
        $pinnacleScreenshot = null;

        if (file_exists($betfairFile)) {
            $screenshot        = file_get_contents($betfairFile);
            $betfairScreenshot = 'data:image/png;base64,' . base64_encode($screenshot);
        }

        if (file_exists($pinnacleFile)) {
            $screenshot         = file_get_contents($pinnacleFile);
            $pinnacleScreenshot = 'data:image/png;base64,' . base64_encode($screenshot);
        }

        return [
            'betfairScreenshot'  => $betfairScreenshot,
            'pinnacleScreenshot' => $pinnacleScreenshot,
        ];
    }
}
