<?php

namespace app\controllers;

use app\components\Parser\Parser;
use app\repositories\MatchRepository;
use Yii;
use yii\web\Controller;
use function class_exists;
use function file_get_contents;
use function ucfirst;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $bookmakerName = 'pinnacle';

        $file = file_get_contents(Yii::getAlias('@runtime') . "/files/$bookmakerName.html");

        $parserStrategyFullClassName = 'app\\components\\Parser\\ParserStrategy\\' . ucfirst($bookmakerName) . 'ParserStrategy';
//        if (!class_exists($parserStrategyFullClassName)) {
//            continue;
//        }

        $parser                = new Parser(new $parserStrategyFullClassName);
        $parsedMatchCollection = $parser->parse($file);

        (new MatchRepository())->save($parsedMatchCollection);

        return $this->render('index');
    }

    /**
     * @return string
     */
    public function actionLastGrabbed(): string
    {
        return $this->render('last-grabbed');
    }
}
