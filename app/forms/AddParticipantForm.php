<?php

namespace app\forms;

use yii\base\Model;

/**
 * Class AddParticipantForm
 * @package app\forms
 */
class AddParticipantForm extends Model
{
    /** @var string */
    public $betfair;

    /** @var string */
    public $pinnacle;

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['betfair', 'pinnacle'], 'required'],
            [['betfair', 'pinnacle'], 'string'],
        ];
    }
}
