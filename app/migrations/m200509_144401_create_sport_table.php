<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sport}}`.
 */
class m200509_144401_create_sport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%sport}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%sport}}');
    }
}
