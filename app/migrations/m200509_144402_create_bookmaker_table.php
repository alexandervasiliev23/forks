<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bookmaker}}`.
 */
class m200509_144402_create_bookmaker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bookmaker}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bookmaker}}');
    }
}
