<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%participant}}`.
 */
class m200509_144419_create_participant_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%participant}}', [
            'id'            => $this->primaryKey(),
            'sport_id'      => $this->integer()->notNull(),
            'betfair_name'  => $this->string(),
            'pinnacle_name' => $this->string(),
            'sbobet_name'   => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-participant-sport_id',
            'participant',
            'sport_id',
            'sport',
            'id'
        );

        $this->createIndex(
            'participant_sport_id_betfair_name_uindex',
            'participant',
            ['sport_id', 'betfair_name'],
            true
        );

        $this->createIndex(
            'participant_sport_id_pinnacle_name_uindex',
            'participant',
            ['sport_id', 'pinnacle_name'],
            true
        );

        $this->createIndex(
            'participant_sport_id_sbobet_name_uindex',
            'participant',
            ['sport_id', 'sbobet_name'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%participant}}');
    }
}
