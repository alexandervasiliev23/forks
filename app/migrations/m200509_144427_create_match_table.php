<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%match}}`.
 */
class m200509_144427_create_match_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%match}}', [
            'id'                      => $this->primaryKey(),
            'bookmaker_id'            => $this->integer()->notNull(),
            'created'                 => $this->dateTime()->defaultValue(new Expression('NOW()')),
            'first_participant_name'  => $this->string()->notNull(),
            'second_participant_name' => $this->string()->notNull(),
            'first_rate'              => $this->float(5)->notNull(),
            'second_rate'             => $this->float(5)->notNull(),
        ]);

        $this->addForeignKey(
            'fk-match-bookmaker_id',
            'match',
            'bookmaker_id',
            'bookmaker',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%match}}');
    }
}
