<?php

namespace app\modules\bookmaker\controllers;

use app\modules\bookmaker\models\Bookmaker;
use Yii;
use yii\base\Action;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;
use const SORT_DESC;

/**
 * Class ApiController
 * @package app\modules\bookmaker\controllers
 */
class ApiController extends Controller
{
    /**
     * @param Action $action
     * @param mixed  $result
     *
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return parent::afterAction($action, $result);
    }

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            [
                'class'   => VerbFilter::class,
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionIndex(): array
    {
        return (new Query())
            ->from(Bookmaker::tableName())
            ->orderBy(['id' => SORT_DESC])
            ->createCommand()
            ->queryAll();
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionCreate(): array
    {
        $bookmaker = new Bookmaker();
        $bookmaker->loadDataFromPost();
        $bookmaker->validateOrDieApi();
        $bookmaker->save();

        Yii::$app->getResponse()->setStatusCode(201);
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return [];
    }
}
