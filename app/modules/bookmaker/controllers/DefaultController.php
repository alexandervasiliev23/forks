<?php

namespace app\modules\bookmaker\controllers;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\bookmaker\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}
