<?php

namespace app\modules\bookmaker\models;

use app\components\ActiveRecord;

/**
 * This is the model class for table "bookmaker".
 *
 * @property int    $id
 * @property string $name
 */
class Bookmaker extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'bookmaker';
    }

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id'   => 'Id',
            'name' => 'Name',
        ];
    }
}
