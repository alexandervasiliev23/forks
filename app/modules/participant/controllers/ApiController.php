<?php

namespace app\modules\participant\controllers;

use app\components\ParticipantsMatcher\ParticipantMatcher;
use app\forms\AddParticipantForm;
use app\modules\bookmaker\models\Bookmaker;
use app\modules\participant\models\Participant;
use Yii;
use yii\base\Action;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\Response;
use function reset;
use const SORT_DESC;

/**
 * Class ApiController
 * @package app\modules\participant\controllers
 */
class ApiController extends Controller
{
    /**
     * @param Action $action
     * @param mixed  $result
     *
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return parent::afterAction($action, $result);
    }

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            [
                'class'   => VerbFilter::class,
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionIndex(): array
    {
        return (new Query())
            ->from(Participant::tableName())
            ->orderBy(['id' => SORT_DESC])
            ->createCommand()
            ->queryAll();
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionCreate(): array
    {
        $participant = new Participant();
        $participant->loadDataFromPost();
        $participant->validateOrDieApi();
        $participant->save();

        Yii::$app->getResponse()->setStatusCode(201);
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return [];
    }
}
