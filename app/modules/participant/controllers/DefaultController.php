<?php

namespace app\modules\participant\controllers;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\participant\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}
