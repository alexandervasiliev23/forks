<?php

namespace app\modules\participant\models;

use app\components\ActiveRecord;
use app\modules\sport\models\Sport;

/**
 * This is the model class for table "participant".
 *
 * @property int         $id
 * @property int         $sport_id
 * @property string|null $betfair_name
 * @property string|null $pinnacle_name
 * @property string|null $sbobet_name
 */
class Participant extends ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'participant';
    }

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['sport_id'], 'required'],
            [['sport_id'], 'default', 'value' => null],
            [['sport_id'], 'integer'],
            [['betfair_name', 'pinnacle_name', 'sbobet_name'], 'string', 'max' => 255],
            [['sport_id', 'betfair_name'], 'unique', 'targetAttribute' => ['sport_id', 'betfair_name']],
            [['sport_id', 'pinnacle_name'], 'unique', 'targetAttribute' => ['sport_id', 'pinnacle_name']],
//            [['sport_id', 'sbobet_name'], 'unique', 'targetAttribute' => ['sport_id', 'sbobet_name']], // todo ограничения Active Record не соответствуют Postgres
            [['sport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sport::class, 'targetAttribute' => ['sport_id' => 'id']],
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id'            => 'ID',
            'sport_id'      => 'Sport ID',
            'betfair_name'  => 'Betfair Name',
            'pinnacle_name' => 'Pinnacle Name',
            'sbobet_name'   => 'Sbobet Name',
        ];
    }
}
