<div id="app">

    <!--    Toast message-->
    <div class="toast toast-success" style="position: absolute; bottom: 2rem; left: 2rem;">
        <div class="toast-body">
            {{ toastMessage }}
        </div>
    </div>
    <!--    Toast message END-->

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#modal">
        Add new participant
    </button>

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new participant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label> Sport
                            <select class="form-control" v-model="sportId">
                                <option v-for="sport in sports" v-bind:value="sport.id">{{ sport.name }}</option>
                            </select>
                        </label>
                    </div>
                    <label>
                        Pinnacle
                        <input type="text" v-model="newPinnacleParticipantName" class="form-control">
                    </label>
                    <label>
                        Betfair
                        <input type="text" v-model="newBetfairParticipantName" class="form-control">
                    </label>
                    <label>
                        Sbobet
                        <input type="text" v-model="newSbobetParticipantName" class="form-control">
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" v-on:click="addNewParticipant()">Add</button>
                </div>
            </div>
        </div>
    </div>

    <ul class="list-group">
        <li class="list-group-item list-group-item-dark">
            <div class="row">
                <div class="col">Sport</div>
                <div class="col">Betfair</div>
                <div class="col">Pinnacle</div>
                <div class="col">Sbobet</div>
            </div>
        </li>
        <li class="list-group-item" v-for="participant in participants">
            <div class="row">
                <div class="col">{{ sports[participant.sport_id].name }}</div>
                <div class="col">{{ participant.betfair_name }}</div>
                <div class="col">{{ participant.pinnacle_name }}</div>
                <div class="col">{{ participant.sbobet_name }}</div>
            </div>
        </li>
    </ul>
</div>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script>
    let app = new Vue({
        el     : '#app',
        data   : {
            participants              : null,
            sports                    : null,
            toastMessage              : null,
            sportId                   : null,
            newBetfairParticipantName : null,
            newPinnacleParticipantName: null,
            newSbobetParticipantName  : null,
        },
        methods: {
            getData() {
                axios.get('/participant/api/index')
                    .then(response => this.participants = response.data)
                axios.get('/sport/api/index')
                    .then(response => this.sports = response.data)
            },
            addNewParticipant() {
                axios.post('/participant/api/create', {
                    'sport_id'     : this.sportId,
                    'betfair_name' : this.newBetfairParticipantName,
                    'pinnacle_name': this.newPinnacleParticipantName,
                    'sbobet_name'  : this.newSbobetParticipantName
                }).then(response => {
                    this.toastMessage = 'New bookmaker created'
                    $('.toast').toast('show')
                    this.getData()
                    $('#modal').modal('hide')
                    this.newBetfairParticipantName = null;
                    this.newPinnacleParticipantName = null;
                    this.newSbobetParticipantName = null;
                    this.sportId = null;
                }).catch(error => {
                    this.toastMessage = 'Error'
                    $('.toast').toast('show')
                })
            }
        },
        mounted() {
            this.getData();
        }
    })
</script>
