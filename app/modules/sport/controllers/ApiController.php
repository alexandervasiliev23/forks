<?php

namespace app\modules\sport\controllers;

use app\modules\sport\models\Sport;
use PDO;
use Yii;
use yii\base\Action;
use yii\db\Exception;
use yii\db\Query;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;
use const SORT_DESC;

/**
 * Class ApiController
 * @package app\modules\sport\controllers
 */
class ApiController extends Controller
{
    /**
     * @param Action $action
     * @param mixed  $result
     *
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return parent::afterAction($action, $result);
    }

    /**
     * @return array|array[]
     */
    public function behaviors(): array
    {
        return [
            [
                'class'   => VerbFilter::class,
                'actions' => [
                    'create' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function actionIndex(): array
    {
        return (new Query())
            ->select([
                'key' => 'id',
                'id',
                'name',
            ])
            ->from(Sport::tableName())
            ->orderBy(['id' => SORT_DESC])
            ->createCommand()
            ->queryAll([PDO::FETCH_UNIQUE|PDO::FETCH_ASSOC]);
    }

    /**
     * @return array
     * @throws HttpException
     */
    public function actionCreate(): array
    {
        $sport = new Sport();
        $sport->loadDataFromPost();
        $sport->validateOrDieApi();
        $sport->save();

        Yii::$app->getResponse()->setStatusCode(201);
        Yii::$app->getResponse()->format = Response::FORMAT_JSON;

        return [];
    }
}
