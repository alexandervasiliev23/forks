<?php

namespace app\modules\sport\controllers;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package app\modules\sport\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex(): string
    {
        return $this->render('index');
    }
}
