<?php

namespace app\modules\sport\models;

use yii\base\Model;

/**
 * Class AddSportForm
 * @package app\modules\sport\models
 */
class AddSportForm extends Model
{
    /** @var string */
    public $name;

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
        ];
    }
}
