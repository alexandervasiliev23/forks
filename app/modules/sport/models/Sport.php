<?php

namespace app\modules\sport\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "sport".
 *
 * @property int    $id
 * @property string $name
 */
class Sport extends \app\components\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return 'sport';
    }

    /**
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * @return array|string[]
     */
    public function attributeLabels(): array
    {
        return [
            'id'   => 'Id',
            'name' => 'Название',
        ];
    }
}
