<div id="app">

    <!--    Toast message-->
    <div class="toast toast-success" style="position: absolute; bottom: 2rem; left: 2rem;">
        <div class="toast-body">
            {{ toastMessage }}
        </div>
    </div>
    <!--    Toast message END-->

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-success mb-3" data-toggle="modal" data-target="#modal">
        Add new sport
    </button>
    <!-- Button trigger modal END -->

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add new participant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label>
                        Name
                        <input type="text" v-model="newSportName" class="form-control">
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" v-on:click="addNewSport()">Add</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal END -->

    <!--    Sports list-->
    <ul class="list-group">
        <li class="list-group-item list-group-item-dark">
            <div class="row">
                <div class="col">Name</div>
            </div>
        </li>
        <li class="list-group-item" v-for="sport in sports">
            <div class="row">
                <div class="col">{{ sport.name }}</div>
            </div>
        </li>
    </ul>
    <!--    Sports list END-->

</div>

<script>
    let app = new Vue({
        el     : '#app',
        data   : {
            sports      : null,
            newSportName: null,
            toastMessage: null
        },
        methods: {
            getData() {
                axios.get('/sport/api/index')
                    .then(response => {
                        this.sports = response.data
                    })
            },
            addNewSport() {
                axios.post('/sport/api/create', {
                    'name': this.newSportName,
                }).then(response => {
                    this.toastMessage = 'New sport created'
                    $('.toast').toast('show')
                    this.getData()
                    $('#modal').modal('hide')
                }).catch(error => {
                    this.toastMessage = 'Error'
                    $('.toast').toast('show')
                }).finally(() => {
                    this.newSportName = null;
                })
            }
        },
        mounted() {
            this.getData();
        }
    })
</script>
