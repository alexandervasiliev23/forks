<?php

namespace app\repositories;

use app\components\Dto\Collections\MatchCollection;
use app\components\Dto\Match;
use app\components\Dto\Participant;
use Yii;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Query;
use function array_keys;

/**
 * Class MatchRepository
 * @package app\repositories
 */
class MatchRepository
{
    private Connection $database;

    /**
     * MatchRepository constructor.
     */
    public function __construct()
    {
        $this->database = Yii::$app->getDb();
    }

    /**
     * @param MatchCollection $matchCollection
     *
     * @throws Exception
     */
    public function save(MatchCollection $matchCollection): void
    {
        $toInsert = [];
        foreach ($matchCollection->getAll() as $match) {
            $toInsert[] = [
                'bookmaker_id'            => $match->getBookmakerId(),
                'first_participant_name'  => $match->getParticipant1()->getName(),
                'second_participant_name' => $match->getParticipant2()->getName(),
                'first_rate'              => $match->getParticipant1()->getRate(),
                'second_rate'             => $match->getParticipant2()->getRate(),
            ];
        }

        if ($toInsert) {
            $this->database
                ->createCommand()
                ->batchInsert('match', array_keys($toInsert[0]), $toInsert)
                ->execute();
        }
    }

    /**
     * @param int $bookmakerId
     *
     * @throws Exception
     */
    public function clear(int $bookmakerId): void
    {
        $this->database->createCommand()
            ->delete('match', [
                'bookmaker_id' => $bookmakerId,
            ])->execute();
    }

    /**
     * @param int $bookmakerId
     *
     * @return MatchCollection
     * @throws Exception
     */
    public function getLastMatchesByBookmaker(int $bookmakerId): MatchCollection
    {
        $lastCreated = (new Query())
            ->from('match')
            ->where(['bookmaker_id' => $bookmakerId])
            ->max('created');


        $matches = (new Query())
            ->from('match')
            ->where([
                'bookmaker_id' => $bookmakerId,
                'created'      => $lastCreated,
            ])
            ->createCommand()
            ->queryAll();

        $matchCollection = new MatchCollection();
        foreach ($matches as $match) {
            $participant1 = new Participant($match['first_participant_name'], $match['first_rate']);
            $participant2 = new Participant($match['second_participant_name'], $match['second_rate']);
            $match        = new Match($participant1, $participant2, $bookmakerId);
            $matchCollection->add($match);
        }

        return $matchCollection;
    }
}
