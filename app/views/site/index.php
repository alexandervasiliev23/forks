<!--    App -->
<div id="app">

    <!--    Add participant-->
    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true"
         style="position: fixed; top: 5rem; left: 1rem;">
        <div class="toast-header">
            <strong class="mr-auto">Add new participant</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="toast-body">
            <ul class="list-group">
                <li class="list-group-item">
                    <small class="text-muted" v-if="participantToAdd.firstBookmaker.bookmakerId">({{
                        getBookmakerName(participantToAdd.firstBookmaker.bookmakerId)
                        }})</small>
                    <span class="m-2">{{ participantToAdd.firstBookmaker.name }}</span>
                </li>
                <li class="list-group-item">
                    <small class="text-muted" v-if="participantToAdd.secondBookmaker.bookmakerId">( {{
                        getBookmakerName(participantToAdd.secondBookmaker.bookmakerId)
                        }})</small>
                    <span class="m-2">{{ participantToAdd.secondBookmaker.name }}</span>
                </li>
                <li class="list-group-item">
                    <label for="sportId"></label><select name="sportId" id="sportId" v-if="data.dictionary" class="form-control" v-model="participantToAdd.sportId">
                        <option v-bind:value="sport.id" v-for="sport in data.dictionary.sports">
                            {{ sport.name }}
                        </option>
                    </select>
                </li>
            </ul>
            <div class="btn btn-success btn-sm m-2 float-right" v-on:click="createNewParticipant">Add</div>
            <div class="btn btn-outline-secondary btn-sm m-2 float-right" v-on:click="clearAddParticipant">Clear</div>
        </div>
    </div>
    <!--    Add participant END-->

    <!--    Forks NEW-->
    <div class="container">
        <h3>Forks NEW</h3>
        <ul class="list-group">
            <li class="list-group-item d-flex justify-content-between align-items-center">
                <ul class="list-group list-group-horizontal-xl flex-grow-1 mr-5">
                    <li class="list-group-item w-50 text-right">
                        <small class="text-muted">(Pinnacle)</small>
                        <span class="m-2">Kiwoom Heroes</span>
                        <span class="badge badge-primary">15.38</span>
                    </li>
                    <li class="list-group-item w-50">
                        <span class="badge badge-primary">1.01</span>
                        <span class="m-2">Kiwoom Heroes</span>
                        <small class="text-muted">(Betfair)</small>
                    </li>
                </ul>
                <span class="badge badge-success badge-pill">+ 4.85 %</span>
            </li>
        </ul>
    </div>
    <!--    Forks NEW ENDs-->

    <!--    Forks -->
    <div class="mb-3">
        <h3 class="text-center">Forks</h3>
        <div href="#" class="card text-center col-4" v-for="fork in data.forks">
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        <span class="badge badge-primary">{{ fork.bookmaker1.participant1.rate }}</span><br>
                        <span>{{ fork.bookmaker1.participant1.name }}</span><br>
                        <small class="text-secondary">({{ getBookmakerName(fork.bookmaker1.bookmakerId) }})</small>
                    </div>
                    <div class="col text-center">
                        <span class="badge badge-primary">{{ fork.bookmaker2.participant1.rate }}</span><br>
                        <span>{{ fork.bookmaker2.participant1.name }}</span><br>
                        <small class="text-secondary">({{ getBookmakerName(fork.bookmaker2.bookmakerId) }})</small>
                    </div>
                </div>
                <p class="d-none">
                    {{ forkProfit = getForkProfit(fork.bookmaker1.participant1.rate,
                    fork.bookmaker2.participant1.rate) }}
                </p>
                <span class="badge badge-success badge-pill" v-if="forkProfit > 0">{{ forkProfit }}%</span>
                <span class="badge badge-secondary badge-pill" v-else>{{ forkProfit }}%</span>
                <br>
                <br>
                <a
                        href="#"
                        class="btn btn-primary btn-sm"
                        v-on:click="calcInit(
                            fork.bookmaker1.participant1.rate,
                            fork.bookmaker2.participant1.rate,
                            fork.bookmaker1.bookmakerId,
                            fork.bookmaker2.bookmakerId
                        )"
                >
                    To calc
                </a>
                <hr>
                <div class="row">
                    <div class="col text-center">
                        <span class="badge badge-primary">{{ fork.bookmaker1.participant2.rate }}</span><br>
                        <span>{{ fork.bookmaker1.participant2.name }}</span><br>
                        <small class="text-secondary">({{ getBookmakerName(fork.bookmaker1.bookmakerId) }})</small>
                    </div>
                    <div class="col text-center">
                        <span class="badge badge-primary">{{ fork.bookmaker2.participant2.rate }}</span><br>
                        <span>{{ fork.bookmaker2.participant2.name }}</span><br>
                        <small class="text-secondary">({{ getBookmakerName(fork.bookmaker2.bookmakerId) }})</small>
                    </div>
                </div>
                <p class="d-none">
                    {{ forkProfit = getForkProfit(fork.bookmaker1.participant2.rate,
                    fork.bookmaker2.participant2.rate) }}
                </p>
                <span class="badge badge-success badge-pill" v-if="forkProfit > 0">{{ forkProfit }}%</span>
                <span class="badge badge-secondary badge-pill" v-else>{{ forkProfit }}%</span>
                <br>
                <br>
                <a
                        href="#"
                        class="btn btn-primary btn-sm"
                        v-on:click="calcInit(
                            fork.bookmaker1.participant2.rate,
                            fork.bookmaker2.participant2.rate,
                            fork.bookmaker1.bookmakerId,
                            fork.bookmaker2.bookmakerId
                        )"
                >
                    To calc
                </a>
            </div>
        </div>
    </div>
    <!--    Forks END -->

    <!--    Calculator-->
    <div class="mb-3">
        <h3 class="text-center">Calculator</h3>
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col"><p>Bookmaker</p></div>
                    <div class="col"><p>Rate</p></div>
                    <div class="col"><p>Bet</p></div>
                    <div class="col"><p>Profit</p></div>
                </div>

                <div class="row">
                    <div class="col">{{ calcBookmaker1 }}</div>
                    <div class="col">
                        <label>
                            <input type="number" v-model="betRate1" class="form-control">
                        </label>
                    </div>
                    <div class="col">
                        <label>
                            <input type="number" v-model="betValue1" v-on:change="calcBetsSum()" class="form-control">
                        </label>
                    </div>
                    <div class="col">{{ calcBetResult(betRate1, betValue1) }}</div>
                </div>

                <div class="row">
                    <div class="col">{{ calcBookmaker2 }}</div>
                    <div class="col">
                        <label>
                            <input type="number" v-model="betRate2" class="form-control">
                        </label>
                    </div>
                    <div class="col">
                        <label>
                            <input type="number" v-model="betValue2" v-on:change="calcBetsSum()" class="form-control">
                        </label>
                    </div>
                    <div class="col">{{ calcBetResult(betRate2, betValue2) }}</div>
                </div>

                <div class="row">
                    <div class="col"></div>
                    <div class="col"></div>
                    <div class="col">
                        <label>
                            <input type="number" v-model="betsSum" v-on:change="calcBetsSizeBySum()"
                                   class="form-control">
                        </label>
                    </div>
                    <div class="col"></div>
                </div>

            </div>
        </div>
    </div>
    <!--    Calculator END-->

    <!--    Not Allocated Matches -->
    <div class="container">
        <h3 class="text-center">Not Allocated Matches</h3>
        <div v-for="(matches, bookmakerId) in data.notAllocatedMatches">
            <h5>{{ getBookmakerName(bookmakerId) }}</h5>
            <div class="row row-cols-3">
                <div class="col mb-2" v-for="match in matches">
                    <div class="card">
                        <div class="card-body">
                            <div>
                                <span class="badge badge-info">{{ match.participant1.rate }}</span>
                                <a href="#" class="text-dark"
                                   v-on:click="addParticipant($event, match, match.participant1)">{{
                                    match.participant1.name }}</a>
                            </div>
                            <div>
                                <span class="badge badge-info">{{ match.participant2.rate }}</span>
                                <a href="#" class="text-dark"
                                   v-on:click="addParticipant($event, match, match.participant2)">{{
                                    match.participant2.name }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--     Not Allocated Matches END -->

</div>
<!--    App END -->

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
        integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
        crossorigin="anonymous"></script>
<script>
    let app = new Vue({
            el      : '#app',
            data    : {
                betRate1        : null,
                betRate2        : null,
                betValue1       : null,
                betValue2       : null,
                betsSum         : null,
                calcBookmaker1  : null,
                calcBookmaker2  : null,
                data            : {
                    dictionary: null
                },
                datetime        : null,
                participantToAdd: {
                    firstBookmaker : {
                        name       : null,
                        bookmakerId: null
                    },
                    secondBookmaker: {
                        name       : null,
                        bookmakerId: null
                    },
                    sportId        : null
                }
            },
            computed: {
                isLoaded() {
                    return this.data.dictionary != null
                }
            },
            methods : {
                roundFloat(float) {
                    return Math.round(float * 100) / 100
                },
                calcBetResult(betRate, betValue) {
                    return this.roundFloat(betValue * (betRate - 1) - (this.betsSum - betValue))
                },
                calcBetsSum() {
                    this.betsSum = this.roundFloat(+this.betValue1 + +this.betValue2)
                },
                calcBetsSizeBySum() {
                    this.betValue1 = this.roundFloat(this.betsSum / (1 + this.betRate1 / this.betRate2))
                    this.betValue2 = this.roundFloat(this.betsSum - this.betValue1)
                },
                calcInit(rate1, rate2, bookmaker1, bookmaker2) {
                    this.betRate1 = rate1
                    this.betRate2 = rate2
                    this.calcBookmaker1 = this.getBookmakerName(bookmaker1)
                    this.calcBookmaker2 = this.getBookmakerName(bookmaker2)
                    this.betsSum = 100
                    this.calcBetsSizeBySum()
                },
                getForkProfit(rate1, rate2) {
                    return this.roundFloat(100 - (1 / rate1 + 1 / rate2) * 100)
                },
                getData() {
                    axios.get('/api/index')
                        .then(response => {
                            this.data = response.data
                        })
                },
                getBookmakerName(id) {
                    if (!this.isLoaded) {
                        return '?';
                    }

                    return this.data.dictionary.bookmakers[id].name;
                },
                addParticipant(event, match, matchParticipant) {
                    event.preventDefault();
                    if (!this.participantToAdd.firstBookmaker.name) {
                        this.participantToAdd.firstBookmaker.name = matchParticipant.name
                        this.participantToAdd.firstBookmaker.bookmakerId = match.bookmakerId
                    } else {
                        this.participantToAdd.secondBookmaker.name = matchParticipant.name
                        this.participantToAdd.secondBookmaker.bookmakerId = match.bookmakerId
                    }
                },
                createNewParticipant() {
                    if (!(
                        this.participantToAdd.firstBookmaker.name
                        && this.participantToAdd.firstBookmaker.bookmakerId
                        && this.participantToAdd.secondBookmaker.name
                        && this.participantToAdd.secondBookmaker.bookmakerId
                        && this.participantToAdd.sportId
                    )) {
                        return null;
                    }
                    if (this.participantToAdd.firstBookmaker.bookmakerId
                        === this.participantToAdd.secondBookmaker.bookmakerId) {
                        return null;
                    }

                    let pinnacle_name;
                    let betfair_name;
                    if (this.participantToAdd.firstBookmaker.bookmakerId === 1) {
                        pinnacle_name = this.participantToAdd.firstBookmaker.name;
                        betfair_name = this.participantToAdd.secondBookmaker.name;
                    } else {
                        pinnacle_name = this.participantToAdd.secondBookmaker.name;
                        betfair_name = this.participantToAdd.firstBookmaker.name;
                    }

                    axios.post('/participant/api/create', {
                        'sport_id'     : this.participantToAdd.sportId,
                        'betfair_name' : betfair_name,
                        'pinnacle_name': pinnacle_name,
                        'sbobet_name'  : null
                    }).then(response => {
                        this.clearAddParticipant();
                    })
                },
                clearAddParticipant() {
                    this.participantToAdd.firstBookmaker.name = null
                    this.participantToAdd.firstBookmaker.bookmakerId = null
                    this.participantToAdd.secondBookmaker.name = null
                    this.participantToAdd.secondBookmaker.bookmakerId = null
                }
            },
            mounted() {
                let toast = $('.toast');
                toast.toast({
                    autohide: false
                })
                toast.toast('show')
                this.getData();

                setInterval(function () {
                    this.getData();
                }.bind(this), 1000)
            }
        }
    );
</script>
