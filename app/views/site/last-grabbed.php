<div id="app">
    <img v-bind:src="data.betfairScreenshot" alt="pinnacleScreenshot">
    <img v-bind:src="data.pinnacleScreenshot" alt="betfairScreenshot">
</div>

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue"></script>
<script>
    let app = new Vue({
        el     : '#app',
        data   : {
            data: null
        },
        methods: {
            getData() {
                axios.get('/api/last-grabbed')
                    .then(response => this.data = response.data)
            },
        },
        mounted() {
            this.getData();
        }
    })
</script>
