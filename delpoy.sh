#!/usr/bin/env bash

cd /var/www/forks

git fetch --all
git reset --hard origin/master
git pull origin master

cd docker-forks
docker-compose up -d --build
#docker start $(docker ps --filter="status=exited" -q)
docker exec php-forks composer install
docker exec php-forks ./yii migrate --interactive=0
root@ubuntu-s-1vcpu-1gb-ams3-01:/var/www/forks#
